package org.fmavlyutov.api.controller;

public interface ICommandController {

    void displayHello();

    void displayGoodbye();

    void displayHelp();

    void displayVersion();

    void displayAbout();

    void displayInfo();

    void displayCommandError();

    void displayArgumentError();

    void displayCommands();

    void displayArguments();

}
