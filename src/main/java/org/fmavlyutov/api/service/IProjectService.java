package org.fmavlyutov.api.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
