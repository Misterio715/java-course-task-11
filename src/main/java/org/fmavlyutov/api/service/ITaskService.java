package org.fmavlyutov.api.service;

import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
