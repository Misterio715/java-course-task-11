package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.ICommandRepository;
import org.fmavlyutov.api.service.ICommandService;
import org.fmavlyutov.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
